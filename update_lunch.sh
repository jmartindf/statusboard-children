#!/bin/bash
VIRTUALENV="statusboard2"
VIRTUALENVDIR="/home/jmartin/.virtualenvs"
SB_DIR="${VIRTUALENVDIR}/${VIRTUALENV}"
DEV_DIR="/home/jmartin/development/statusboard"

cd $DEV_DIR
source "${SB_DIR}/bin/activate"
make deploy_lunch 1>/dev/null
