HTML = `pwd`/html
LUNCH = ./lunch.py
CHORES = ./makechores.py
CHORESNEW = $(HOME)/scripts/ChoresWeb
DEVDIR = /home/jmartin/www/status

lunch:
	$(LUNCH) > $(HTML)/lunch.html

chores:
	$(CHORES) > $(HTML)/whodoeswhat.html	

choresnew:
	$(CHORESNEW)

rsync:
	rsync -rav $(HTML)/ $(DEVDIR)

deploy_lunch: lunch rsync

deploy_chores: choresnew rsync

deploy_all: lunch choresnew rsync
