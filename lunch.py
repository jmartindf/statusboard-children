#!/usr/bin/env python
from bs4 import BeautifulSoup, element
import requests
import html
import arrow
from requests.packages.urllib3.exceptions import NewConnectionError

def formatEntry(entry):
	if entry is None:
		return
	tomorrow = BeautifulSoup(entry.content.contents[0], features='lxml')
	lines = []
	if not tomorrow.b or not tomorrow.b.string:
		print("<h1>Hot Lunch</h1>\n")
		print("<ul><li class=\"fun\">Nothing</li></ul>\n")
		return
	if "Elementary" in tomorrow.b.string.strip():
		print ("<h1>Hot Lunch</h1>")
		# Fix the date entry
		titleStr = soup.entry.title.string.replace("Sept ","Sep ")
		dateStr = ("%s, %s" % (titleStr, arrow.now().year))
		try:
			date = arrow.get(dateStr,[
				"MMMM DD, YYYY",
				"MMMM D, YYYY",
				"MMM DD, YYYY",
				"MMM D, YYYY",
				"MM/D, YYYY",
				"MM/DD, YYYY",
				"M/DD, YYYY",
				"M/D, YYYY"]
			)
		except arrow.parser.ParserError:
			date = arrow.now()
		lunchDate = date.format("dddd, MMMM D")
		print ("<p class=\"sb-light-grey dateline\">for %s</p>\n<ul>" % lunchDate)
		for line in tomorrow.b.next_siblings:
			if isinstance(line,element.Tag):
				if line.name == "b":
					break
			elif isinstance(line,element.NavigableString):
				lines.append(line)
		menu = " ".join(lines)
		items = menu.split(" or ")
		for item in items:
			print("<li class=\"fun\">%s</li>" % html.escape(item))
		print ("</ul>\n")

header = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Tomorrow's Lunch</title>
<meta data-refresh-every-n-seconds="300" application-name="Hot Lunch" data-allows-resizing="YES" data-default-size="6,6" data-min-size="4,3" data-max-size="12,12" data-allows-scrolling="NO" />
<link rel="stylesheet" href="martin-styles.css" type="text/css" media="screen" />
</head>

<body>
"""

footer = """
</body>
</html>"""

try:
	r = requests.get("http://oregonlunch.blogspot.com/feeds/posts/default")
	soup = BeautifulSoup(r.text, features='xml')

	print(header)
	formatEntry(soup.entry)
	print(footer)
except (ConnectionError, NewConnectionError) as err:
	pass

# entries = soup.find_all('entry')
# for entry in entries:
# 	formatEntry(entry)


