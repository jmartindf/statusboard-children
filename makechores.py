#!/usr/bin/env python3

from string import Template
from datetime import date

template_html = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Who Does What?</title>
	<meta charset="UTF-8" />
	<link rel="stylesheet" href="martin-styles.css" type="text/css" media="screen" />
	
	<meta application-name="Who Does It?" data-refresh-every-n-seconds="900" data-allows-resizing="Yes" data-default-size="16,6" data-min-size="4,1" data-max-size="16,16" data-allows-scrolling="NO" />
</head>

<body>

<h1 style="font-size: 4rem;"><span style="font-size: 4rem;">${name_set}</span> set the table</h1>
<h1 style="font-size: 4rem;"><span style="font-size: 4rem;">${name_clear}</span> clear the table and do the dishes</h1>

</body>
</html>
"""

template_set = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Dinner Chores</title>
	<meta charset="UTF-8" />
	<link rel="stylesheet" href="martin-styles.css" type="text/css" media="screen" />

	<meta application-name="Table Setting" data-refresh-every-n-seconds="300" data-allows-resizing="Yes" data-default-size="8,5" data-min-size="4,1" data-max-size="16,16" data-allows-scrolling="NO" />

</head>

<body>

<h1><span id="daughter">${name_set}</span> Set the Table</h1>
<ul>
	<li class="chores">Clear the table</li>
	<li class="chores">Wipe down the table</li>
	<li class="chores">Sweep the floor</li>
	<li class="chores">Set the table</li>
	<li class="chores">Get drinks for everyone</li>
</ul>

</body>
</html>
"""

template_clear = """<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Dinner Chores</title>
	<meta charset="UTF-8" />
	<link rel="stylesheet" href="martin-styles.css" type="text/css" media="screen" />

	<meta application-name="Table Clearing" data-refresh-every-n-seconds="900" data-allows-resizing="Yes" data-default-size="8,6" data-min-size="4,1" data-max-size="16,16" data-allows-scrolling="NO" />

</head>

<body>

<h1><span id="daughter">${name_clear}</span> Clear the Table</h1>
<ul>
	<li class="chores">Clear the table</li>
	<li class="chores">Wipe down the table</li>
	<li class="chores">Sweep the floor</li>
	<li class="chores">Do the dishes</li>
</ul>

</body>
</html>
"""

assignments = dict(
	Monday = ['Bethany', 'Katherine'],
	Tuesday = ['Amy', 'Esther'],
	Wednesday = ['Esther', 'Katherine'],
	Thursday = ['Amy', 'Bethany'],
	Friday = ['Katya', 'Esther'],
	Saturday = ['Esther', 'Bethany'],
)

sundays = [
	['Bethany','Amy'],
	['Bethany','Amy'],
]

tmpl = Template(template_html)
tmpl_set = Template(template_set)
tmpl_clear = Template(template_clear)

day = date.today().strftime("%A")

if day != "Sunday":
	names = dict(name_set=assignments[day][0],name_clear=assignments[day][1])
	print( tmpl.substitute(names) )
	with open("html/dinner-clear.html", "w") as f:
		f.write( tmpl_clear.substitute(names) )
	with open("html/dinner-set.html", "w") as f:
		f.write( tmpl_set.substitute(names) )
else:
	sunday = date.today().toordinal() % 2
	names = dict(name_set=sundays[sunday][0],name_clear=sundays[sunday][1])
	print( tmpl.substitute(names) )
	with open("html/dinner-clear.html", "w") as f:
		f.write( tmpl_clear.substitute(names) )
	with open("html/dinner-set.html", "w") as f:
		f.write( tmpl_set.substitute(names) )
