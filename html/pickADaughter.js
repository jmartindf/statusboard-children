function pickADaughter(type) {
	var daughter = "";
	var dt = new Date();
	var dayOfMonth = dt.getDate();
	var dayOfWeek = dt.getDay() + 1; // 1 = Sunday, 2 = Monday, etc
	switch (dayOfWeek) {
		case 3:
		case 5:
		case 7:
			daughter = "Esther";
			break;
		case 2:
		case 4:
		case 6:
			daughter = "Katherine";
			break;
		default:
			if ((dayOfMonth % 2)==0) { // Even days
				daughter = "Esther";
			} else { // Odd days
				daughter = "Katherine";
			}
			break;
	}
	if (type==2) {
		daughter=="Esther" ? daughter="Katherine" : daughter = "Esther";
	}
	return daughter;
}

function nameDaughter(type) {
	document.getElementById("daughter").innerHTML = pickADaughter(type);
}
