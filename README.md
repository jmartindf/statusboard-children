# statusboard-children
[DIY Panels](https://library.panic.com/status-board/diy-panels/) for [Panic's Status Board](https://panic.com/statusboard) to tell my children their upcoming schedule and assigned chores.

Note: [Wallpaper can't be shared](https://library.panic.com/status-board/sb-wallpaper/).

